CXX = gcc
CFLAGS = 
INCPATH = 
LIBS = -lX11 -lXtst -lpthread 
LIBPATH = 
SRC = client_handler.c fake_key.c server.c sock_utils.c string_utils.c 
OBJ = $(SRC:.c=.o)
TARGET = server


$(TARGET): $(OBJ)
	$(CXX) $(LIBPATH) $(LIBS) $(OBJ) -o $@


client_handler.o: client_handler.c sock_utils.h string_utils.h fake_key.h client_handler.h
	$(CXX) $(CFLAGS) $(INCPATH) -c $<

fake_key.o: fake_key.c fake_key.h
	$(CXX) $(CFLAGS) $(INCPATH) -c $<

server.o: server.c sock_utils.h client_handler.h
	$(CXX) $(CFLAGS) $(INCPATH) -c $<

sock_utils.o: sock_utils.c
	$(CXX) $(CFLAGS) $(INCPATH) -c $<

string_utils.o: string_utils.c string_utils.h
	$(CXX) $(CFLAGS) $(INCPATH) -c $<

clean:
	rm -f $(OBJ) $(TARGET)

run:
	make --quiet
	./$(TARGET)

remake:
	make clean;
	make $(TARGET);
tar:
	make clean; tar -cvf TARBALL.tar.gz *

