
#ifndef FAKE_KEY_H
#define FAKE_KEY_H

#include <X11/Xlib.h>

static unsigned Pressed_Keys[40];
static unsigned Key_Counter = 0;


void get_mouse_pos(Display *display, Window *root, int *x, int *y);
void mouse_to(int x,int y);
void shift_mouse(int x, int y);
void mouse_up(int button);
void mouse_down(int button);
void mouse_click(int button);

void press_key(unsigned key);
void release_key(unsigned key);
void release_all_keys();
void press_combo(const unsigned keys[], unsigned size);

#endif
