
#ifndef STRING_UTILS_H
#define STRING_UTILS_H

/*Return position of substr match*/
int str_find(const char* substr, const char* str);

/*core of str_find*/
int _str_find(const char* substr, const char* str, int pos);

/*Return subset of str*/
char* str_subset(const char* str, unsigned indx, unsigned count);

/*Return str with characters erased*/
char* str_erase(const char* str, unsigned indx, unsigned count);

/*Returns the number of times substr occurs in str*/
int str_count(const char* substr, const char* str);

#endif
