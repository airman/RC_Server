
#ifndef MAX_CMD
#define MAX_CMD 11
#endif

#ifndef CLIENT_HANDLER_H
#define CLIENT_HANDLER_H


/*executes command based on command index passed and char* */
int exec_command(unsigned, const char*);

/*Gets command index from char* */
int get_command(const char*);

/*Handles client requests*/
void* client_handler(void*);

#endif
