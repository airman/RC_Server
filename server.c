
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>

#include "sock_utils.h"
#include "client_handler.h"

/*Simple Multi-threaded server */

#define MAX_CONNECTIONS 100

static int port;
static int serversock;

static pthread_t server_thread;
static pthread_t client_threads[5];

static int client_socks[MAX_CONNECTIONS];
static int client_count =0;

int catch_signal(int sig, void (*handler)(int)){
    struct sigaction action;
    action.sa_handler = handler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    
    return sigaction(sig, &action, NULL);
}


void* server_handler(void *args){
    serversock = socket(AF_INET, SOCK_STREAM, 0);
    bind_to_port(serversock, port);
    if (listen(serversock, 5) == -1){
        error("Can't listen");
    }

    printf("Listening on port %i\n", port);
    puts("Waiting for connection");
    while(client_count < MAX_CONNECTIONS){
        struct sockaddr_storage client_addr;
        unsigned int address_size = sizeof(client_addr);
        int sock = accept(serversock, 
                (struct sockaddr*) &client_addr, &address_size);
        client_socks[client_count] = sock;
        pthread_create(&client_threads[client_count], NULL, 
                client_handler, (void *) &sock);
        client_count ++;
        /*printf("Connections Made: %i\n", client_count);*/
    }
    printf("Max connections made.\n");
    int i;
    for(i=0; i < MAX_CONNECTIONS; i++){
        pthread_join(client_threads[i], NULL);
    }
    printf("Quitting server\n");
    close(serversock);
    return NULL;
}

void quit(int sig){
     
    printf("\nQuitting...\n");
    //terminate client threads
    int i= 0;
    for(; i < client_count; i++){
        pthread_cancel(client_threads[i]);
        close(client_socks[i]);
    }

    //terminate server threads
    pthread_cancel(server_thread);
    close(serversock);
    exit(0);
}

void run_server(int p){
    port = p;
    catch_signal(SIGINT, quit);
    catch_signal(SIGTERM, quit);
    catch_signal(SIGQUIT, quit);
    pthread_create(&server_thread, NULL, server_handler, NULL);
    pthread_join(server_thread, NULL);
}

int main(int argc, char** argv){ 
    if(argc > 1){ 
        int p = atoi(argv[1]);
        if( p > 1000){
            run_server(p);
            return 0;
        }
        puts("Port must be above 1000, switching to default port 9000");
    }

    run_server(9000);
    return 0;
}
