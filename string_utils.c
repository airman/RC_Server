
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "string_utils.h"

int str_find(const char* substr, const char* str){
    return _str_find(substr, str, 0);
}

int _str_find(const char* substr, const char* str, int pos){
    if (substr == NULL || str == NULL)
        return -1;
    unsigned len_sub = strlen(substr);
    unsigned len_str = strlen(str);
    if (len_sub > len_str)
        return -1;
    
    char *sub = strndup(str, len_sub);
    if (strcmp(sub, substr) == 0){
        return pos;
    }
    return _str_find(substr, strndup(str+1, len_str-1), pos+1);
}

char* str_subset(const char* str, unsigned indx, unsigned count){
    if (str == NULL)
        return NULL;
    int len = strlen(str);
    if (indx >= len || count > len)
        return NULL;
    char *result = strndup(str + indx, count);
    return result;
}

char* str_erase(const char* str, unsigned indx, unsigned count){
    if (str == NULL)
        return NULL;
    int len = strlen(str);
    if ( indx >= len || count > len)
        return NULL;
    if(indx== 0){
        return str_subset(str, count, len - count);
    }
    int result_len = strlen(str) - (count - indx);
    char *before = strndup(str, indx); 
    char *after = strndup(str + indx + count, result_len);

    return strcat(before, after);
}

int str_count(const char* substr, const char* str){
    if (substr == NULL || str == NULL)
        return -1;
    int pos = str_find(substr, str);
    if (pos == -1)
        return 0; 

    int len_sub = strlen(substr);
    char *next = str_subset(str, pos+1, strlen(str) - len_sub);
    if (next != NULL)
        return 1 + str_count(substr, next);
    return 1;
}
