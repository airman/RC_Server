
#ifndef SOCK_UTILS_H
#define SOCK_UTILS_H

/*Functions for typical C socket programming*/

///displays error message and exits with exit code 1
void error(char *msg);

///open socket for client
int open_socket(char *host, char *port);


///bind to port
void bind_to_port(int sock, int port);

///wrapper function for send()
int send_msg(int sock, char *msg);

///wrapper function for recv(), reads all bytes received
int recv_msg(int sock, char *buf, int len);

#endif
