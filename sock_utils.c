
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

void error(char *msg){
    fprintf(stderr, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

int open_socket(char *host, char *port)
{
    struct addrinfo *res;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    if (getaddrinfo(host, port, &hints, &res) == -1)
        error("Can't resolve the address");
    int d_sock = socket(res->ai_family, res->ai_socktype,
            res->ai_protocol);
    if (d_sock == -1)
        error("Can't open socket");
    int c = connect(d_sock, res->ai_addr, res->ai_addrlen);
    freeaddrinfo(res);
    if (c == -1)
        error("Can't connect to socket");
    return d_sock;
}


void bind_to_port(int sock, int port){
    struct sockaddr_in host;
    host.sin_family = AF_INET;
    host.sin_port = (in_port_t)htons(port);
    host.sin_addr.s_addr = htonl(INADDR_ANY);
    
    int reuse = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, 
                (char*) &reuse, sizeof(int)) == -1){
        error("Can't  set the reuse option on the socket");
    }
    int c = bind(sock, (struct sockaddr *) &host, sizeof(host));
    if (c == -1){
        error("Can't bind to socket");
    }
}

int send_msg(int sock, char *msg){
    int result = send(sock, msg, strlen(msg), 0);
    if (result == -1){
        fprintf(stderr, "%s: %s\n", "Error Sending to Client",
                strerror(errno));
    }
    return result;
}

int recv_msg(int sock, char *buf, int len){
    char *msg = buf;
    int slen = len;
    int c = recv(sock, msg, slen, 0);
    while((c > 0) && (msg[c-1] != '\n')){
        msg += c;
        slen -= c;
        c = recv(sock, msg, slen, 0);
    }
    if (c < 0){
        return c;
    } else if( c==0){
        ///send empty str
        buf[0] = '\0';
    } else{
        ///replace '\r' with null char
        msg[c-1] = '\0';
    }
    return len - slen;
}
