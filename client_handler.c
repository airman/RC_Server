
#ifndef CLIENT_HANDLER_H
#define CLIENT_HANDLER_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <unistd.h>

#include "sock_utils.h"
#include "string_utils.h"
#include "fake_key.h"

#include "client_handler.h"

static const char *COMMANDS[MAX_CMD] = {"KEYDOWN", "KEYUP", 
    "KEYCOMB", "MSHIFT", "MMOVE", "MDOWN", "MUP", "MCLICK", "MDOUBLECLICK", "EXEC", 
    "KRELEASE"};

int get_command(const char* str){
    int i; 
    for(i=0; i < MAX_CMD; i++){
        if (str_find(COMMANDS[i], str) > -1)
            return i;
    } 
    return -1; 
}

int exec_command(unsigned cmd, const char* str){
    char* args = str_erase(str, 0, strlen(COMMANDS[cmd]) + 1);
    unsigned keys[4];
    unsigned count = 0;
    switch(cmd){
        case 0:///key press
            press_key(atoi(args));
            break;

        case 1:///key release
            release_key(atoi(args));
            break;

        case 2: ///key combo
            while(args != NULL || strlen(args) > 1){
                int pos = str_find(" ", args);
                if (pos > -1){
                    char *k = str_subset(args, 0, pos);  
                    keys[count++] = atoi(k);
                    args = str_erase(args, 0, strlen(k)+1);
                }else{
                    keys[count++] = atoi(args);
                    break;
                }
            }
            press_combo(keys, count);
            break;

        case 3: // mouse shift
            {
                int pos = str_find(" ", args);
                char *sub = str_subset(args, 0, pos);
                int x = atoi(sub);
                int y = atoi(str_erase(args, 0, strlen(sub)+1));
                
                shift_mouse(x, y);
            }
            break;
        case 4: ///mouse move
            {
                int pos = str_find(" ", args);
                char *sub = str_subset(args, 0, pos);
                int x = atoi(sub);
                int y = atoi(str_erase(args, 0, strlen(sub)+1));
                
                mouse_to(x, y);
            }
            break;

        case 5: ///mouse button down
            puts("mouse button down");
            mouse_down(atoi(args));
            break;

        case 6: ///mouse button up
            mouse_up(atoi(args));
            break;

        case 7: ///mouse click 
            mouse_click(atoi(args));
            break;

        case 8: ///mouse double click 
            mouse_click(atoi(args));
            mouse_click(atoi(args));
            break;

        case 9: ///exec
            system(args);
            break;

        case 10: ///release all keys
            puts("release all keys");
            release_all_keys();
            break;
    } 
    return 0;
}

void* client_handler(void* p_sock){
    int sock =  *(int*)p_sock;
    int size = 40;
    char buffer[size];
    char *END_MSG = strdup("quit");
    printf("Client connected\n");   
    while(strcmp(buffer, END_MSG) != 0){
        if (recv_msg(sock,buffer, size) < 0){
            fprintf(stderr, "%s: %s\n", "Socket Error", strerror(errno));
            break;
        } 
        if (strlen(buffer) < 1){
            printf("Client Ended Connection.\n");
            break;
        }
        int cmd = get_command(buffer);
        printf("Client: '%s'\n", buffer);
        if (cmd > -1)
            exec_command(cmd, buffer);
    }

    close(sock);
    return NULL;
}


#endif
