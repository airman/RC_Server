#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "fake_key.h"


void mouse_down(int button){
	Display *display = XOpenDisplay(NULL);
	XEvent event;
	
	if(display == NULL)
	{
		fprintf(stderr, "Failed to get display!\n");
		exit(EXIT_FAILURE);
	}
	
    XTestFakeButtonEvent(display, button, True, CurrentTime);
    XFlush(display);
    XCloseDisplay(display);
}

void mouse_up(int button){
	Display *display = XOpenDisplay(NULL);
	
	if(display == NULL)
	{
		fprintf(stderr, "Failed to get display!\n");
		exit(EXIT_FAILURE);
	}
	
    XTestFakeButtonEvent(display, button, False, CurrentTime);
    XFlush(display);
    XCloseDisplay(display);
}

void mouse_click(int button){
    mouse_down(button);
	usleep(100000);
    mouse_up(button);
}

/*get mouse pos*/
void get_mouse_pos(Display *display, Window *root, int *x, int *y){
    Window root_window;
    Window window_returned;
    int win_x, win_y;
    unsigned int mask_return;
    XQueryPointer(display, *root, &window_returned,
            &window_returned, x, y, &win_x, &win_y,
            &mask_return);
}

/*move mouse*/
void mouse_to(int x,int y)
{
	Display *display = XOpenDisplay(0);
	Window root = DefaultRootWindow(display);
	XWarpPointer(display, None, root, 0, 0, 0, 0, x, y);
	XFlush(display);
	XCloseDisplay(display);
}

/*shift mouse to position, adds (x, y) to current mouse position*/
void shift_mouse(int x, int y){
    int init_x, init_y;
	Display *display = XOpenDisplay(0);
	Window root = DefaultRootWindow(display);
    get_mouse_pos(display, &root, &init_x, &init_y);
	XWarpPointer(display, None, root, 0, 0, 0, 0, x + init_x, y + init_y);
	XFlush(display);
	XCloseDisplay(display);
}

/*Press a single key down*/
void press_key(unsigned key){
    Display *display = XOpenDisplay(0);
    unsigned key_code = XKeysymToKeycode(display, key);
    
    ///return if key is already pressed
    int i; 
    for(i=0; i < Key_Counter; i++){
        if (key_code == Pressed_Keys[i]){
            XTestFakeKeyEvent(display, key_code, False, 0);
            return;
        }
    }

    ///press key
    XTestFakeKeyEvent(display, key_code, True, 0);
    ///add key to Pressed_Keys
    Pressed_Keys[Key_Counter++] = key_code; 
}

/*release a single key*/
void release_key(unsigned key){
    Display *display = XOpenDisplay(0);
    unsigned key_code = XKeysymToKeycode(display, key);
    int i;
    for(i=0; i < Key_Counter; i++){
        if (key_code == Pressed_Keys[i]){
            XTestFakeKeyEvent(display, key_code, False, 0);
            Pressed_Keys[i] = 0; 
            break;
        }
    }
}


/*release all pressed keys in reverse order*/
void release_all_keys(){
    Display *display = XOpenDisplay(0);
    int i;
    for(i=Key_Counter-1; i > -1; i--){
        if (Pressed_Keys[i] != 0){
            XTestFakeKeyEvent(display, Pressed_Keys[i], False, 0);
            Pressed_Keys[i] = 0; 
        }
    }
}

/*press and immediately release a combination of keys*/
void press_combo(const unsigned keys[], unsigned size){
    Display *display = XOpenDisplay(0);
    unsigned key_codes[size];
    int i;
    for(i=0; i < size; i++){
        key_codes[i] = XKeysymToKeycode(display, keys[i]); 
        ///press key
        XTestFakeKeyEvent(display, key_codes[i], True, 0);
    }

    ///release keys
    for (i=size-1; i > -1; i--){
        XTestFakeKeyEvent(display, key_codes[i], False, 0);
    }
    XFlush(display);
    XCloseDisplay(display);
}

